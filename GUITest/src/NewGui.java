
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class NewGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewGui frame = new NewGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public NewGui() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblHalloWelt = new JLabel("Hallo Welt!");
		lblHalloWelt.setBounds(48, 35, 170, 14);
		contentPane.add(lblHalloWelt);
		
		JButton btnRed = new JButton("Red");
		btnRed.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnRed_clicked();
			}
		});
		
		btnRed.setBounds(38, 84, 89, 23);
		contentPane.add(btnRed);
		
		JButton btnBlue = new JButton("Blue");
		btnBlue.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnBlue_clicked();
			}
		});
		
		btnBlue.setBounds(38, 114, 89, 23);
		contentPane.add(btnBlue);
		
		JButton btnGreen = new JButton("Green");
		btnGreen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnGreen_clicked();
			}
		});
		
		btnGreen.setBounds(38, 144, 89, 23);
		contentPane.add(btnGreen);
		
		JButton btnReset = new JButton("Reset");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnReset_clicked();
			}
		});
		
		btnReset.setBounds(38, 174, 89, 23);
		contentPane.add(btnReset);
	}
	
	public void btnRed_clicked() {
		this.contentPane.setBackground(Color.RED);
	}
		
	public void btnBlue_clicked() {
		this.contentPane.setBackground(Color.BLUE);
	}
	
	public void btnGreen_clicked() {
		this.contentPane.setBackground(Color.GREEN);
	}
	
	public void btnReset_clicked() {
		this.contentPane.setBackground(Color.WHITE);
	}

}
